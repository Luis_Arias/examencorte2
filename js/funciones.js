var Columneo= 0;
var Peso= 0.0;
var Nivel="";
var Altura=0.0;
var Masa = 0.0;
var Edad =0;
var imcTotal= 0;
var numRegistros= 0;

function Generacion()
{
    let txtAltura = document.getElementById('txtAltura');
    let txtEdad =document.getElementById('txtEdad');
    let txtIMC = document.getElementById('txtIMC');
    let txtPeso = document.getElementById('txtPeso');
    let txtNivel =document.getElementById('txtNivel');
    Edad = Math.floor(Math.random()*99)+18;
    Peso = Math.floor(Math.random()*110)+20;
    Altura = ((Math.random()*1)+1.5).toFixed(1)
    txtAltura.value = Altura;
    txtEdad.value = Edad;
    txtPeso.value = Peso;
    txtNivel.value = "";
    txtIMC.value = "";
}

function Registro() 
{ 
    let txtAltura = document.getElementById('txtAltura');
    let txtEdad =document.getElementById('txtEdad');
    let txtIMC = document.getElementById('txtIMC');
    let txtPeso = document.getElementById('txtPeso');
    let txtNivel =document.getElementById('txtNivel');
    let Resultados = document.getElementById('Resultados');
	if ( txtEdad.value === "" || txtAltura.value === "" || txtPeso.value === "" || txtIMC.value === "" || txtNivel.value === "") {
		return alert("Rellene los campos faltantes");
	}
	
	var imcConvertido = parseFloat(txtIMC.value);
	if (isNaN(imcConvertido)) {
        return alert("IMC invalido");
    }
	numRegistros = numRegistros + 1;
	imcTotal = imcTotal + imcConvertido;
	document.getElementById("Promedio").innerHTML = (imcTotal / numRegistros).toFixed(2);
    Resultados.innerHTML += `<tr>
		<td>${numRegistros}</td>
		<td>${txtEdad.value}</td>
		<td>${txtAltura.value}</td>
		<td>${txtPeso.value}</td>
		<td>${txtIMC.value}</td>
		<td>${txtNivel.value}</td>
	</tr>`;
}

function IMC()
{
    let txtIMC = document.getElementById('txtIMC');
    let txtNivel = document.getElementById('txtNivel');

    Masa = (Peso/(Altura*Altura)).toFixed(1);

    if(Masa < 18.5)
    {
        Nivel = "Bajo peso";
    }
    else if(Masa >=18.5 && Masa < 25)
    {
        Nivel = "Peso ideal";
    }
    else if(Masa>= 25 && Masa < 30)
    {
        Nivel="Sobrepeso";
    }
    else if(Masa>=30)
    {
        Nivel="Obesidad";
    }

    txtIMC.value = Masa;
    txtNivel.value = Nivel;
}

function Borrar()
{
    let txtAltura = document.getElementById('txtAltura');
    let txtEdad =document.getElementById('txtEdad');
    let txtIMC = document.getElementById('txtIMC');
    let txtPeso = document.getElementById('txtPeso');
    let txtNivel =document.getElementById('txtNivel');
    let Resultados = document.getElementById('Resultados');
    let Promedio = document.getElementById('Promedio');

    Resultados.innerHTML = "";
            Columneo=0;
            Promedio.innerHTML ="";
            numRegistros= 0;
            imcTotal= 0;
            txtAltura.value = Altura;
            txtEdad.value = Edad;
            txtIMC.value= "";
            txtNivel= "";
            txtPeso= Peso;
}